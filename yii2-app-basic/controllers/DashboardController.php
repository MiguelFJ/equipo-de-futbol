<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Dashboard;
use Yii;

class DashboardController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreateTeam()
    {
        $model = new Dashboard();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->set('team1', [
                $model->jugador1local,
                $model->jugador2local,
                $model->jugador3local,
                $model->jugador4local,
                $model->jugador5local,
                $model->jugador6local,
                $model->jugador7local,
                $model->jugador8local,
                $model->jugador9local,
                $model->jugador10local,
                $model->jugador11local,
            ]);

            Yii::$app->session->set('team2', [
                $model->jugador1visitante,
                $model->jugador2visitante,
                $model->jugador3visitante,
                $model->jugador4visitante,
                $model->jugador5visitante,
                $model->jugador6visitante,
                $model->jugador7visitante,
                $model->jugador8visitante,
                $model->jugador9visitante,
                $model->jugador10visitante,
                $model->jugador11visitante,
            ]);

            return $this->redirect(['main']);
        }

        return $this->render('create-team', ['model' => $model]);
    }

    public function actionMain()
    {
        $team1Players = Yii::$app->session->get('team1', []);
        $team2Players = Yii::$app->session->get('team2', []);

        return $this->render('main-page', [
            'team1Players' => $team1Players,
            'team2Players' => $team2Players,
        ]);
    }

}

