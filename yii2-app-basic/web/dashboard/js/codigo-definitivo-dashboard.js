//LISTA DE JUGADORES EQUIPO 1
document.addEventListener("DOMContentLoaded", function() {
    const team1TableBody = document.querySelector(".alturaminima-fichequipo-uno tbody");
    if (team1TableBody) {
        team1Players.forEach(player => {
            const row = document.createElement("tr");
            const cell = document.createElement("td");
            cell.textContent = player;
            row.appendChild(cell);
            team1TableBody.appendChild(row);
        });
    }
});


//LISTA DE JUGADORES EQUIPO 2
document.addEventListener("DOMContentLoaded", function() {
    const team2TableBody = document.querySelector(".alturaminima-fichequipo-dos tbody");
    if (team2TableBody) {
        team2Players.forEach(player => {
            const row = document.createElement("tr");
            const cell = document.createElement("td");
            cell.textContent = player;
            row.appendChild(cell);
            team2TableBody.appendChild(row);
        });
    }
});


//CONTADOR DE TIEMPO
document.addEventListener("DOMContentLoaded", function() {
    console.log("Timer script loaded.");

    const elementoContador = document.querySelector(".timer");

    let duracion = 90 * 60; // 90 minutes in seconds

    // Move intervaloContador outside the function to avoid reinitialization
    const intervaloContador = setInterval(updateTimer, 1000);

    function updateTimer() {
        let horas = Math.floor(duracion / 3600);
        let minutos = Math.floor((duracion % 3600) / 60);
        let segundos = duracion % 60; // Corrected variable name

        horas = horas < 10 ? "0" + horas : horas;
        minutos = minutos < 10 ? "0" + minutos : minutos;
        segundos = segundos < 10 ? "0" + segundos : segundos;

        elementoContador.textContent = horas + ":" + minutos + ":" + segundos;

        if (duracion > 0) {
            duracion--;
        } else {
            clearInterval(intervaloContador);
        }
    }

    // Initial call to updateTimer
    updateTimer();
});


//EVENTOS ALEATORIOS

let currentPlayerWithBall = null;
const matchEvents = document.querySelector(".alturaminima-eventos tbody");
let team1Score = 0;
let team2Score = 0;
const team1ScoreElement = document.querySelector(".alturaminima-temporiz .team-row .score-one");
const team2ScoreElement = document.querySelector(".alturaminima-temporiz .team-row .score-two");

function logEvent(event) {
    const row = document.createElement("tr");
    const cell = document.createElement("td");
    cell.textContent = event;
    row.appendChild(cell);
    matchEvents.appendChild(row);
    console.log(event); // For debugging
}

function getRandomPlayer(teamPlayers) {
    return teamPlayers[Math.floor(Math.random() * teamPlayers.length)];
}

function getRandomPlayer(teamPlayers) {
    return teamPlayers[Math.floor(Math.random() * teamPlayers.length)];
}

function passBall(fromPlayer, toPlayer) {
    currentPlayerWithBall = toPlayer;
    const event = fromPlayer + " le pasa el balón a " + toPlayer + ".";
    console.log(fromPlayer + " le pasa el balón a " + toPlayer + ".");
    logEvent(event);
}

function stealBall(stealer, victim) {
    currentPlayerWithBall = stealer;
    const event = stealer + " le quita el balón a " + victim + ".";
    console.log(stealer + " le quita el balón a " + victim + ".");
    logEvent(event);
}

function shootAtGoal(shooter, isTeam1) {
    const goal = Math.random() < 0.5; // 50% chance of scoring
    if (goal) {
        const event = shooter + " chuta el balón a la portería contraria, ¡y mete gol!";
        logEvent(event);
        if (isTeam1) {
            team1Score++;
            team1ScoreElement.textContent = team1Score; //quiero que el texto de numero de goles cambie
        } else {
            team2Score++;
            team2ScoreElement.textContent = team2Score; //quiero que el texto de numero de goles cambie
        }
    } else {
        const event = shooter + " chuta el balón a la portería contraria... y falla.";
        logEvent(event);
    }
    currentPlayerWithBall = null; // Ball is with no one after a shot
}

function shootOutOfBounds(shooter) {
    const event = shooter + " chuta el balón fuera del campo.";
    logEvent(event);
    currentPlayerWithBall = null; // Ball is with no one after out of bounds
}

function handleMatchOutcome(team1Score, team2Score) {
    const event = "Fin del partido.";
    logEvent(event);
    if (team1Score > team2Score) {
        logEvent("¡El equipo 1 es el ganador!");
    } else if (team1Score < team2Score) {
        logEvent("¡El equipo 2 es el ganador!");
    } else {
        logEvent("El partido ha terminado en empate.");
    }
}

function simulateMatchEvents(team1Players, team2Players) {
    const initialTeam = Math.random() < 0.5 ? team1Players : team2Players;
    currentPlayerWithBall = getRandomPlayer(initialTeam);
    logEvent(currentPlayerWithBall + " empieza con el balón.");

    const matchInterval = setInterval(() => {
        if (currentPlayerWithBall === null) {
            const randomTeam = Math.random() < 0.5 ? team1Players : team2Players;
            currentPlayerWithBall = getRandomPlayer(randomTeam);
            logEvent(currentPlayerWithBall + " recoge el balón.");
        } else {
            const isTeam1 = team1Players.includes(currentPlayerWithBall);
            const fromPlayer = currentPlayerWithBall;
            const action = Math.random();
            
            if (action < 0.4) { // 40% chance to pass
                const teamPlayers = isTeam1 ? team1Players : team2Players;
                let toPlayer;
                do {
                    toPlayer = getRandomPlayer(teamPlayers);
                } while (toPlayer === fromPlayer);
                passBall(fromPlayer, toPlayer);
            } else if (action < 0.6) { // 20% chance to steal
                const opposingTeam = isTeam1 ? team2Players : team1Players;
                const stealer = getRandomPlayer(opposingTeam);
                stealBall(stealer, fromPlayer);
            } else if (action < 0.8) { // 20% chance to shoot at goal
                shootAtGoal(fromPlayer, isTeam1);
            } else { // 20% chance to shoot out of bounds
                shootOutOfBounds(fromPlayer);
            }
        }
    }, 3000); // Trigger an event every 3 seconds

    // Stop the match after 90 minutes (5400 seconds)
    setTimeout(() => {
        clearInterval(matchInterval);
        handleMatchOutcome();
    }, 5400000); // 5400 * 1000 milliseconds
}

// Start simulating match events
document.addEventListener("DOMContentLoaded", function() {
    simulateMatchEvents(team1Players, team2Players);
});
