<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Estadios $model */

$this->title = '¿Qué datos del estadio vas a cambiar?';
$this->params['breadcrumbs'][] = ['label' => 'Estadios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="estadios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
