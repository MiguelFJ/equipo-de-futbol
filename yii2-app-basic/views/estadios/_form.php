<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Estadios $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="estadios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'provincia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dia')->textInput() ?>

    <?= $form->field($model, 'hora_entrada')->textInput() ?>

    <?= $form->field($model, 'hora_salida')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
