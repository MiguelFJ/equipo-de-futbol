<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Estadios $model */

$this->title = 'Introduce los datos del nuevo estadio';
$this->params['breadcrumbs'][] = ['label' => 'Estadios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estadios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
