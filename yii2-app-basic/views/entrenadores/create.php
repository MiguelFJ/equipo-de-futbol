<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Entrenadores $model */

$this->title = 'Introduce los datos del nuevo entrenador';
$this->params['breadcrumbs'][] = ['label' => 'Entrenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
