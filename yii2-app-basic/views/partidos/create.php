<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Partidos $model */

$this->title = 'Introduce los datos del nuevo partido';
$this->params['breadcrumbs'][] = ['label' => 'Partidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partidos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
