<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

$this->title = 'Simulación';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="dashboard">
        <div class="widget">
            <p>Aquí podrás simular un partido de fútbol.</p>
            <p>Esto no es más que una muestra de lo que se puede hacer.</p>
        </div>

        <p>
            <?= Html::a('Nueva partida', ['dashboard/create-team'], ['class' => 'btn btn-success color-icono-crear']) ?>
        </p>

        <div class="scoreboard">
        <div class="row fila1">
            <div class="col-sm-6 col-md">
                <div class="card alturaminima-temporiz">
                <div class="card-body tarjeta">
                        <div class="team-row">
                        <div class="team">
                            <h4>Equipo 1</h4>
                        </div>
                        <div class="team">
                            <h4>Número de goles: <span class="score">02</span></h4>
                        </div>
                        <div class="team">
                            <h4>Tiempo: <span class="timer">00:34:05</span></h4>
                        </div>
                        <div class="team">
                            <h4>Número de goles: <span class="score">01</span></h4>
                        </div>
                        <div class="team">
                            <h4>Equipo 2</h4>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <h2 class="text-center">Eventos del partido</h2>
        <div class="player-stats">
            <div class="row fila1">
                <div class="col-sm-6 col-md">
                    <div class="card alturaminima-fichequipo-uno">
                        <div class="card-body tarjeta"> 
                            <h4>EQUIPO 1</h4>
                            <table>
                                <thead>
                                    <tr>
                                        <h5>Jugadores</h5>
                                        <p>jugador1</p>
                                        <p>jugador2</p>
                                        <p>jugador3</p>
                                        <p>jugador4</p>
                                        <p>jugador5</p>
                                        <p>jugador6</p>
                                        <p>jugador7</p>
                                        <p>jugador8</p>
                                        <p>jugador9</p>
                                        <p>jugador10</p>
                                        <p>jugador11</p>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Player statistics will be dynamically added here -->

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                                <!-- Match events will be dynamically added here -->
                <div class="col-sm-6 col-md">
                    <div class="card alturaminima-eventos-ex">
                        <div class="card-body tarjeta"> 
                            <h4>EVENTOS</h2>
                            <h6> El jugador 1 del equipo 1 le pasa el balón al jugador 3 del equipo 1.</h6>
                            <h6> El jugador 4 del equipo 2 le quita el balón al jugador 3 del equipo 1.</h6>
                            <h6> El jugador 4 del equipo 1 le pasa el balón al jugador 6 del equipo 2.</h6>
                            <h6> El jugador 6 del equipo 2 mete gol en la portería del equipo 1.</h6>
                            <h6> El equipo 2 gana un punto.</h6>
                        </div>
                    </div>
                </div> 

                <div class="col-sm-6 col-md">
                    <div class="card alturaminima-fichequipo-dos">
                        <div class="card-body tarjeta"> 
                            <h4>EQUIPO 2</h4>
                            <table>
                                <thead>
                                    <tr>
                                    <h5>Jugadores</h5>
                                        <p>jugador1</p>
                                        <p>jugador2</p>
                                        <p>jugador3</p>
                                        <p>jugador4</p>
                                        <p>jugador5</p>
                                        <p>jugador6</p>
                                        <p>jugador7</p>
                                        <p>jugador8</p>
                                        <p>jugador9</p>
                                        <p>jugador10</p>
                                        <p>jugador11</p>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Player statistics will be dynamically added here -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </div>

    </div>
    <!-- Add more widgets as needed -->

</div>