<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Añade a los jugadores de ambos equipos';
?>

<div class="dashboard-create-allteams">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'team-form',
        'method' => 'post',
        'action' => ['dashboard/create-team']
    ]); ?>

<!--EQUIPO 1-->
    <h2>Aquí estarán los jugadores del equipo 1: </h2>
    <?= $form->field($model, 'jugador1local')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador2local')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador3local')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador4local')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador5local')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador6local')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador7local')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador8local')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador9local')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador10local')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador11local')->textInput(['maxlength' => true]) ?>
<!--FIN EQUIPO 1-->

<!--EQUIPO 2-->
    <h2>Aquí estarán los jugadores del equipo 2: </h2>
    <?= $form->field($model, 'jugador1visitante')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador2visitante')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador3visitante')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador4visitante')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador5visitante')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador6visitante')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador7visitante')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador8visitante')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador9visitante')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador10visitante')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'jugador11visitante')->textInput(['maxlength' => true]) ?>
<!--FIN EQUIPO 2-->

    <div class="form-group">
        <?= Html::submitButton('Siguiente', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
