<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

$this->title = 'Simulación';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('@web/dashboard/js/codigo-definitivo-dashboard.js', ['depends' => [\yii\web\JqueryAsset::class]]);

$this->registerJs("
    const team1Players = " . json_encode($team1Players) . ";
    const team2Players = " . json_encode($team2Players) . ";
", \yii\web\View::POS_HEAD);
?>

<div class="dashboard-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="dashboard">
        <div class="widget">
            <p>¿Cómo terminará este partido simulado?</p>
        </div>


        <div class="scoreboard">
        <div class="row fila1">
            <div class="col-sm-6 col-md">
                <div class="card alturaminima-temporiz">
                <div class="card-body tarjeta">
                        <div class="team-row">
                        <div class="team">
                            <h4>Equipo 1</h4>
                        </div>
                        <div class="team">
                            <h4>Número de goles: <span class="score-one">0</span></h4>
                        </div>
                        <div class="team">
                            <h4>Tiempo: <span class="timer">01:30:00</span></h4>
                        </div>
                        <div class="team">
                            <h4>Número de goles: <span class="score-two">0</span></h4>
                        </div>
                        <div class="team">
                            <h4>Equipo 2</h4>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <h2 class="text-center">Eventos del partido</h2>
        <div class="player-stats">
            <div class="row fila1">
                <div class="col-sm-6 col-md">
                    <div class="card alturaminima-fichequipo-uno">
                        <div class="card-body tarjeta"> 
                            <h2>EQUIPO 1</h2>
                            <table style="margin-left: 25px;">
                                <thead>
                                    <tr>
                                        <th>Jugadores</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md">
                    <div class="card alturaminima-eventos">
                        <div class="card-body tarjeta"> 
                        <h4>EVENTOS</h4>
                        <table style="margin-left: 95px;">
                                <thead>
                                    <tr>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 

                <div class="col-sm-6 col-md">
                    <div class="card alturaminima-fichequipo-dos">
                        <div class="card-body tarjeta"> 
                            <h2>EQUIPO 2</h2>
                            <table style="margin-left: 25px;">
                                <thead>
                                    <tr>
                                        <th>Jugadores</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </div>

    </div>
</div>
