<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Goles $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="goles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'localidad_gol')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'minuto')->textInput() ?>

    <?= $form->field($model, 'local')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <!-- <?= $form->field($model, 'partido_id')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
