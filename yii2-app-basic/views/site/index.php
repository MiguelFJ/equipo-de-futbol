<?php

/** @var yii\web\View $this */

use yii\bootstrap4\Html;

$this->title = 'Fútbase';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <div id="encabezado-logotipo">
        <div class="encabezado-logotipo">
            <?= Html::img('@web/images/logoDefinitivoFutbase.png', ['width' => '750'], ['heigth' => '590']) ?>
        </div>
        <div>
    
        <div id="encabezado">
            <h1 class="display-4">FÚTBASE</h1>
        </div>

        <div id="encabezado">
            <h1 class="display-4">¡Bienvenido a la mejor aplicación de fútbol!</h1>
        </div>

        <!--imagen-->
        <div id="margenInicial" class="margenInicial">
            <?= Html::img('@web/images/imageFutbol.png', ['width' => '640'], ['heigth' => '490']) ?>
        </div>
        <!--fin de la imagen-->
        
        <div id="texto">
            <p class="lead">Si te encanta el fútbol, esta aplicación es para tí.</p>
        </div>

    </div>

    <div class="body-content">
        <!-- primera fila-->
        
        <div class="row fila-tarjetas">
            <div class="col-sm-6 col-md">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3 class="text-center">Lista de jugadores</h3>
                        <p class="text-center">Podrás ver a todos los jugadores:</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::img('@web/images/imagenJugadoresFutbol.png',  ['width' => '340'], ['heigth' => '340']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3 class="text-center">Lista de entrenadores</h3>
                        <p class="text-center">Podrás ver a todos los entrenadores:</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::img('@web/images/imagenEntrenadoresFutbol.png',  ['width' => '340'], ['heigth' => '340']) ?>
                        </p>
                    </div>
                </div>
            </div>
        
        </div>
        
        <div class="row fila-tarjetas">
            <div class="col-sm-6 col-md">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3 class="text-center">Lista de estadios</h3>
                        <p class="text-center">Podrás ver todos los estadios:</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::img('@web/images/imagenEstadiosFutbol.png',  ['width' => '340'], ['heigth' => '340']) ?>
                        </p>
                    </div>
                </div>
            </div>
        <!--fin de la primera fila-->
        
        <!--segunda fila-->
            <div class="col-sm-6 col-md">
               <div class="card alturaminima">
                   <div class="card-body tarjeta">
                        <h3 class="text-center">Lista de partidos</h3>
                        <p class="text-center">Podrás ver todos los partidos que se han jugado:</p>
                        <p class="d-flex justify-content-center">
                            <?= Html::img('@web/images/imagenPartidosFutbol.png',  ['width' => '340'], ['heigth' => '340']) ?>
                        </p>
                   </div>
               </div>
           </div>
        </div>

        <div class="row fila-tarjetas">
            <div class="col-sm-6 col-md">
               <div class="card alturaminima">
                   <div class="card-body tarjeta">
                       <h3 class="text-center">Lista de goles</h3>
                       <p class="text-center">Podrás ver todos los goles marcados:</p>
                       <p class="d-flex justify-content-center">
                            <?= Html::img('@web/images/imagenGolesFutbol.png',  ['width' => '340'], ['heigth' => '340']) ?>
                       </p>
                   </div>
               </div>
           </div>

           <div class="col-sm-6 col-md">
               <div class="card alturaminima">
                   <div class="card-body tarjeta">
                       <h3 class="text-center">Simulación</h3>
                       <p class="text-center">Podrás simular partidos de fútbol:</p>
                       <p class="d-flex justify-content-center">
                            <?= Html::img('@web/images/imageDashboardFutbol.png',  ['width' => '340'], ['heigth' => '340']) ?>
                       </p>
                   </div>
               </div>
           </div>
        <!--fin de la segunda fila-->
        </div>
    
    </div>
</div>