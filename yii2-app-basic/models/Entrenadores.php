<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property int $id
 * @property string $nombre
 * @property string|null $apellidos
 * @property int|null $edad
 * @property int|null $num_annios
 * @property int|null $id_apellidos_entr
 * @property int|null $estadio_id
 *
 * @property ApellidosEntre $apellidosEntr
 * @property Estadios $estadio
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['edad', 'num_annios', 'id_apellidos_entr', 'estadio_id'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['apellidos'], 'string', 'max' => 50],
            [['id_apellidos_entr'], 'exist', 'skipOnError' => true, 'targetClass' => ApellidosEntre::class, 'targetAttribute' => ['id_apellidos_entr' => 'idapellentre']],
            [['estadio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estadios::class, 'targetAttribute' => ['estadio_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'edad' => 'Edad',
            'num_annios' => 'Num Annios',
            'id_apellidos_entr' => 'Id Apellidos Entr',
            'estadio_id' => 'Estadio ID',
        ];
    }

    /**
     * Gets query for [[ApellidosEntr]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApellidosEntr()
    {
        return $this->hasOne(ApellidosEntre::class, ['idapellentre' => 'id_apellidos_entr']);
    }

    /**
     * Gets query for [[Estadio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadio()
    {
        return $this->hasOne(Estadios::class, ['id' => 'estadio_id']);
    }
}
