<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apellidos_jugad".
 *
 * @property int $idapelljugad
 * @property string|null $nombre
 * @property string|null $apellidos
 *
 * @property Jugadores[] $jugadores
 */
class ApellidosJugad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apellidos_jugad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idapelljugad'], 'required'],
            [['idapelljugad'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['apellidos'], 'string', 'max' => 50],
            [['idapelljugad'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idapelljugad' => 'Idapelljugad',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
        ];
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::class, ['id_apellidos_juga' => 'idapelljugad']);
    }
}
