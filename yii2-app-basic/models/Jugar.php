<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugar".
 *
 * @property int $idjugar
 * @property int|null $idjugadores
 * @property int|null $idpartidos
 *
 * @property Jugadores $idjugadores0
 * @property Partidos $idpartidos0
 */
class Jugar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjugadores', 'idpartidos'], 'integer'],
            [['idjugadores'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['idjugadores' => 'id']],
            [['idpartidos'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::class, 'targetAttribute' => ['idpartidos' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idjugar' => 'Idjugar',
            'idjugadores' => 'Idjugadores',
            'idpartidos' => 'Idpartidos',
        ];
    }

    /**
     * Gets query for [[Idjugadores0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores0()
    {
        return $this->hasOne(Jugadores::class, ['id' => 'idjugadores']);
    }

    /**
     * Gets query for [[Idpartidos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpartidos0()
    {
        return $this->hasOne(Partidos::class, ['id' => 'idpartidos']);
    }
}
