<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ocurrir".
 *
 * @property int $idocurrir
 * @property int|null $idgoles
 * @property int|null $idpartidos
 *
 * @property Goles $idgoles0
 * @property Partidos $idpartidos0
 */
class Ocurrir extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ocurrir';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idgoles', 'idpartidos'], 'integer'],
            [['idgoles'], 'exist', 'skipOnError' => true, 'targetClass' => Goles::class, 'targetAttribute' => ['idgoles' => 'id']],
            [['idpartidos'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::class, 'targetAttribute' => ['idpartidos' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idocurrir' => 'Idocurrir',
            'idgoles' => 'Idgoles',
            'idpartidos' => 'Idpartidos',
        ];
    }

    /**
     * Gets query for [[Idgoles0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdgoles0()
    {
        return $this->hasOne(Goles::class, ['id' => 'idgoles']);
    }

    /**
     * Gets query for [[Idpartidos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpartidos0()
    {
        return $this->hasOne(Partidos::class, ['id' => 'idpartidos']);
    }
}
