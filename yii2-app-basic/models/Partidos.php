<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $id
 * @property string|null $localidad_partido
 * @property string|null $fecha
 * @property string|null $hora_inicio
 * @property string|null $hora_fin
 * @property int $ganador
 *
 * @property Jugar[] $jugars
 * @property Ocurrir[] $ocurrirs
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora_inicio', 'hora_fin'], 'safe'],
            [['ganador'], 'required'],
            [['ganador'], 'integer'],
            [['localidad_partido'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'localidad_partido' => 'Localidad Partido',
            'fecha' => 'Fecha',
            'hora_inicio' => 'Hora Inicio',
            'hora_fin' => 'Hora Fin',
            'ganador' => 'Ganador',
        ];
    }

    /**
     * Gets query for [[Jugars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugars()
    {
        return $this->hasMany(Jugar::class, ['idpartidos' => 'id']);
    }

    /**
     * Gets query for [[Ocurrirs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOcurrirs()
    {
        return $this->hasMany(Ocurrir::class, ['idpartidos' => 'id']);
    }
}
