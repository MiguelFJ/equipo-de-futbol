<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $edad
 * @property string|null $nacionalidad
 * @property int $numero
 * @property int|null $id_nacionalidad
 * @property int|null $id_apellidos_juga
 * @property int|null $estadio_id
 *
 * @property ApellidosJugad $apellidosJuga
 * @property Estadios $estadio
 * @property Jugar[] $jugars
 * @property Meter[] $meters
 * @property Nacionalidad $nacionalidad0
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['edad', 'numero', 'id_nacionalidad', 'id_apellidos_juga', 'estadio_id'], 'integer'],
            [['numero'], 'required'],
            [['nombre', 'nacionalidad'], 'string', 'max' => 20],
            [['apellidos'], 'string', 'max' => 50],
            [['id_nacionalidad'], 'exist', 'skipOnError' => true, 'targetClass' => Nacionalidad::class, 'targetAttribute' => ['id_nacionalidad' => 'idnacionalidad']],
            [['id_apellidos_juga'], 'exist', 'skipOnError' => true, 'targetClass' => ApellidosJugad::class, 'targetAttribute' => ['id_apellidos_juga' => 'idapelljugad']],
            [['estadio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estadios::class, 'targetAttribute' => ['estadio_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'edad' => 'Edad',
            'nacionalidad' => 'Nacionalidad',
            'numero' => 'Numero',
            'id_nacionalidad' => 'Id Nacionalidad',
            'id_apellidos_juga' => 'Id Apellidos Juga',
            'estadio_id' => 'Estadio ID',
        ];
    }

    /**
     * Gets query for [[ApellidosJuga]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApellidosJuga()
    {
        return $this->hasOne(ApellidosJugad::class, ['idapelljugad' => 'id_apellidos_juga']);
    }

    /**
     * Gets query for [[Estadio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadio()
    {
        return $this->hasOne(Estadios::class, ['id' => 'estadio_id']);
    }

    /**
     * Gets query for [[Jugars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugars()
    {
        return $this->hasMany(Jugar::class, ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Meters]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMeters()
    {
        return $this->hasMany(Meter::class, ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Nacionalidad0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNacionalidad0()
    {
        return $this->hasOne(Nacionalidad::class, ['idnacionalidad' => 'id_nacionalidad']);
    }
}
