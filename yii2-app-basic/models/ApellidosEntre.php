<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apellidos_entre".
 *
 * @property int $idapellentre
 * @property string|null $nombre
 * @property string|null $apellidos
 *
 * @property Entrenadores[] $entrenadores
 */
class ApellidosEntre extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apellidos_entre';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idapellentre'], 'required'],
            [['idapellentre'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['apellidos'], 'string', 'max' => 50],
            [['idapellentre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idapellentre' => 'Idapellentre',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::class, ['id_apellidos_entr' => 'idapellentre']);
    }
}
