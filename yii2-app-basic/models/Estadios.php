<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estadios".
 *
 * @property int $id
 * @property string|null $provincia
 * @property string|null $dia
 * @property string|null $hora_entrada
 * @property string|null $hora_salida
 *
 * @property Entrenadores[] $entrenadores
 * @property Jugadores[] $jugadores
 */
class Estadios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estadios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dia', 'hora_entrada', 'hora_salida'], 'safe'],
            [['provincia'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'provincia' => 'Provincia',
            'dia' => 'Dia',
            'hora_entrada' => 'Hora Entrada',
            'hora_salida' => 'Hora Salida',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::class, ['estadio_id' => 'id']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::class, ['estadio_id' => 'id']);
    }
}
