<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meter".
 *
 * @property int $idmeter
 * @property int|null $idjugadores
 * @property int|null $idgoles
 *
 * @property Goles $idgoles0
 * @property Jugadores $idjugadores0
 */
class Meter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjugadores', 'idgoles'], 'integer'],
            [['idjugadores'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['idjugadores' => 'id']],
            [['idgoles'], 'exist', 'skipOnError' => true, 'targetClass' => Goles::class, 'targetAttribute' => ['idgoles' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmeter' => 'Idmeter',
            'idjugadores' => 'Idjugadores',
            'idgoles' => 'Idgoles',
        ];
    }

    /**
     * Gets query for [[Idgoles0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdgoles0()
    {
        return $this->hasOne(Goles::class, ['id' => 'idgoles']);
    }

    /**
     * Gets query for [[Idjugadores0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores0()
    {
        return $this->hasOne(Jugadores::class, ['id' => 'idjugadores']);
    }
}
