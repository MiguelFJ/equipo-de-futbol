<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goles".
 *
 * @property int $id
 * @property string|null $localidad_gol
 * @property string|null $minuto
 * @property int $local
 * @property string|null $fecha
 * @property int|null $partido_id
 *
 * @property Meter[] $meters
 * @property Ocurrir[] $ocurrirs
 */
class Goles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['minuto', 'fecha'], 'safe'],
            [['local'], 'required'],
            [['local', 'partido_id'], 'integer'],
            [['localidad_gol'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'localidad_gol' => 'Localidad Gol',
            'minuto' => 'Minuto',
            'local' => 'Local',
            'fecha' => 'Fecha',
            'partido_id' => 'Partido ID',
        ];
    }

    /**
     * Gets query for [[Meters]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMeters()
    {
        return $this->hasMany(Meter::class, ['idgoles' => 'id']);
    }

    /**
     * Gets query for [[Ocurrirs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOcurrirs()
    {
        return $this->hasMany(Ocurrir::class, ['idgoles' => 'id']);
    }
}
