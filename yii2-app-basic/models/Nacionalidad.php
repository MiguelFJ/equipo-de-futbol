<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nacionalidad".
 *
 * @property int $idnacionalidad
 * @property string|null $nombre
 * @property string|null $nacionalidad
 *
 * @property Jugadores[] $jugadores
 */
class Nacionalidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nacionalidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idnacionalidad'], 'required'],
            [['idnacionalidad'], 'integer'],
            [['nombre', 'nacionalidad'], 'string', 'max' => 20],
            [['idnacionalidad'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idnacionalidad' => 'Idnacionalidad',
            'nombre' => 'Nombre',
            'nacionalidad' => 'Nacionalidad',
        ];
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::class, ['id_nacionalidad' => 'idnacionalidad']);
    }
}
