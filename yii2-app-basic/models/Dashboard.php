<?php

namespace app\models;

use yii\base\Model;

class Dashboard extends Model
{
    public $jugador1local;
    public $jugador2local;
    public $jugador3local;
    public $jugador4local;
    public $jugador5local;
    public $jugador6local;
    public $jugador7local;
    public $jugador8local;
    public $jugador9local;
    public $jugador10local;
    public $jugador11local;
    public $jugador1visitante;
    public $jugador2visitante;
    public $jugador3visitante;
    public $jugador4visitante;
    public $jugador5visitante;
    public $jugador6visitante;
    public $jugador7visitante;
    public $jugador8visitante;
    public $jugador9visitante;
    public $jugador10visitante;
    public $jugador11visitante;

    public function rules()
    {
        return [
            [['jugador1local', 'jugador2local', 'jugador3local', 'jugador4local', 'jugador5local', 'jugador6local', 'jugador7local', 'jugador8local', 'jugador9local', 'jugador10local', 'jugador11local', 'jugador1visitante', 'jugador2visitante', 'jugador3visitante', 'jugador4visitante', 'jugador5visitante', 'jugador6visitante', 'jugador7visitante', 'jugador8visitante', 'jugador9visitante', 'jugador10visitante', 'jugador11visitante'], 'required'],
            [['jugador1local', 'jugador2local', 'jugador3local', 'jugador4local', 'jugador5local', 'jugador6local', 'jugador7local', 'jugador8local', 'jugador9local', 'jugador10local', 'jugador11local', 'jugador1visitante', 'jugador2visitante', 'jugador3visitante', 'jugador4visitante', 'jugador5visitante', 'jugador6visitante', 'jugador7visitante', 'jugador8visitante', 'jugador9visitante', 'jugador10visitante', 'jugador11visitante'], 'string', 'max' => 45],
        ];
    }

    public function attributeLabels()
    {
        return [
            // Team 1 players
            'jugador1local' => 'Jugador 1 del equipo 1',
            'jugador2local' => 'Jugador 2 del equipo 1',
            'jugador3local' => 'Jugador 3 del equipo 1',
            'jugador4local' => 'Jugador 4 del equipo 1',
            'jugador5local' => 'Jugador 5 del equipo 1',
            'jugador6local' => 'Jugador 6 del equipo 1',
            'jugador7local' => 'Jugador 7 del equipo 1',
            'jugador8local' => 'Jugador 8 del equipo 1',
            'jugador9local' => 'Jugador 9 del equipo 1',
            'jugador10local' => 'Jugador 10 del equipo 1',
            'jugador11local' => 'Jugador 11 del equipo 1',

            // Team 2 players
            'jugador1visitante' => 'Jugador 1 del equipo 2',
            'jugador2visitante' => 'Jugador 2 del equipo 2',
            'jugador3visitante' => 'Jugador 3 del equipo 2',
            'jugador4visitante' => 'Jugador 4 del equipo 2',
            'jugador5visitante' => 'Jugador 5 del equipo 2',
            'jugador6visitante' => 'Jugador 6 del equipo 2',
            'jugador7visitante' => 'Jugador 7 del equipo 2',
            'jugador8visitante' => 'Jugador 8 del equipo 2',
            'jugador9visitante' => 'Jugador 9 del equipo 2',
            'jugador10visitante' => 'Jugador 10 del equipo 2',
            'jugador11visitante' => 'Jugador 11 del equipo 2',
        ];
    }
}
